package com.example.daniel.appbibliotecaapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView txtcod;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtcod = (TextView) findViewById(R.id.textViewCod);
        Intent i = getIntent();
        txtcod.setText(i.getStringExtra("txtCod"));
    }

    public void ScanCodBar(View view) {
        Intent intent = new Intent("br.com.livroandroid.hellocodereader");
        startActivity(intent);
    }

    public void btnSMS(View view) {
        Uri uri = Uri.parse("sms:81 988518787");
        Intent intent = new Intent(Intent.ACTION_SENDTO,uri);
        intent.putExtra("sms_body","Cod:");
        startActivity(intent);
    }
}
